#pragma once

#include "Picture.h"
#include <list>
#include <memory>


class Tag
{
public:
	Tag() = default;
	Tag(int picID, int userID);

	int getUserID() const;
	void setUserID(int newUserID);

	int getPictureID() const;
	void setPictureID(int newPicID);

private:
	int m_pictureID;
	int m_userID;
};
