#include "DatabaseAccess.h"
#pragma warning(disable:4996)


bool DatabaseAccess::open()
{
	std::string dbFileName = "MyDB.sqlite";

	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->_db);

	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
	}

	if (doesFileExist == -1)
	{
		//initiate database.
		std::cout << "Database not found, creating it now." << std::endl;

		const char* sqlCreateUsersStatement = "CREATE TABLE USERS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL);";
		this->execCommand(sqlCreateUsersStatement);

		const char* sqlCreateAlbumsStatement = "CREATE TABLE ALBUMS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, USER_ID INTEGER NOT NULL, FOREIGN KEY (USER_ID) REFERENCES USERS(ID));";
		this->execCommand(sqlCreateAlbumsStatement);

		const char* sqlCreatePicturesStatement = "CREATE TABLE PICTURES(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));";
		this->execCommand(sqlCreatePicturesStatement);

		const char* sqlCreateTagsStatement = "CREATE TABLE TAGS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL, FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID), FOREIGN KEY(USER_ID) REFERENCES USERS(ID));";
		this->execCommand(sqlCreateTagsStatement);
	}
	return true;
}


void DatabaseAccess::close()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}


void DatabaseAccess::clear()
{
	const char* clearCmd = "BEGIN;\
		DELETE FROM PICTURES;\
		DELETE FROM ALBUMS;\
		DELETE FROM TAGS;\
		COMMIT;";
	this->execCommand(clearCmd);
}


const std::list<Album> DatabaseAccess::getAlbums()
{
	stringstream cmd;
	cmd << "SELECT * FROM ALBUMS;";

	std::list<Album> listOfAlbums;
	this->execSelectCmd(cmd.str(), this->albumsCallback, &listOfAlbums);

	return listOfAlbums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	stringstream cmd;
	cmd << "SELECT * FROM ALBUMS where USER_ID=" << user.getId() << ";";

	std::list<Album> listOfAlbums;
	execSelectCmd(cmd.str(), &this->albumsCallback, &listOfAlbums);

	return listOfAlbums;
}


void DatabaseAccess::createAlbum(const Album& album)
{
	stringstream cmd;
	cmd << "INSERT INTO ALBUMS(NAME, CREATION_DATE, USER_ID) VALUES('" << album.getName() << "','" << album.getCreationDate() << "'," << album.getOwnerId() << ");";
	this->execCommand(cmd.str());
}


void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	stringstream cmd;
	cmd << "BEGIN;" <<
		"DELETE FROM TAGS WHERE PICTURE_ID IN(SELECT ID FROM PICTURES WHERE ALBUM_ID = ((SELECT ID FROM ALBUMS WHERE ALBUMS.NAME = '" << albumName << "' AND USER_ID = " << userId << ")));" <<
		"DELETE FROM PICTURES WHERE ALBUM_ID = (SELECT ID FROM ALBUMS WHERE USER_ID = " << userId << " AND ALBUMS.NAME = '" << albumName << "');" <<
		"DELETE FROM ALBUMS WHERE USER_ID = " << userId << " AND NAME = '" << albumName << "';" <<
		"COMMIT;";
	this->execCommand(cmd.str());
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	stringstream cmd;
	cmd << "SELECT * FROM ALBUMS where USER_ID=" << userId << " and NAME='" << albumName << "';";

	std::list<Album> listOfAlbums;
	execSelectCmd(cmd.str(), &this->albumsCallback, &listOfAlbums);

	return listOfAlbums.size() != 0;
}


Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	std::list<Album> listOfAlbums;

	stringstream cmd;
	cmd << "SELECT * FROM ALBUMS where NAME='" << albumName << "';";
	execSelectCmd(cmd.str(), &this->albumsCallback, &listOfAlbums);

	Album album(listOfAlbums.begin()->getOwnerId(), listOfAlbums.begin()->getName(), listOfAlbums.begin()->getCreationDate());

	stringstream cmd2;
	cmd2 << "SELECT * FROM PICTURES where ALBUM_ID=" <<
		"(SELECT ID FROM ALBUMS WHERE NAME ='" << albumName << "');";
	std::list<Picture> listOfPictures;
	execSelectCmd(cmd2.str(), &this->picturesCallback, &listOfPictures);

	for (auto p : listOfPictures) 
	{
		stringstream cmd3;
		cmd3 << "SELECT * FROM TAGS where PICTURE_ID=" << p.getId() << ";";
		std::list<Tag> listOfTags;
		execSelectCmd(cmd3.str(), &this->tagsCallback, &listOfTags);

		for (auto t : listOfTags)
			p.tagUser(t.getUserID());

		album.addPicture(p);
	}

	return album;
}


void DatabaseAccess::closeAlbum(Album& pAlbum)
{
	pAlbum.getPictures().clear();
}

void DatabaseAccess::printAlbums()
{
	std::list<Album> allAlbums = this->getAlbums();
	std::cout << "There are: " << allAlbums.size() << " Albums." << std::endl;
	for (std::list<Album>::iterator i = allAlbums.begin(); i != allAlbums.end(); i++)
	{
		std::cout << "Album '" << i->getName() << "' Created By User with id " << i->getOwnerId() << std::endl;
	}
}


void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	stringstream cmd;
	cmd << "INSERT INTO PICTURES(NAME,LOCATION, CREATION_DATE,ALBUM_ID) VALUES ('" << picture.getName() <<
		"','" << picture.getPath() << "','" << picture.getCreationDate() <<
		"',(SELECT ID FROM ALBUMS WHERE ALBUMS.NAME='" << albumName << "'));";
	this->execCommand(cmd.str());
}


void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	stringstream cmd;
	cmd << "BEGIN;" <<
		"DELETE FROM TAGS WHERE PICTURE_ID = (SELECT ID FROM PICTURES WHERE NAME = '" << pictureName << "' AND ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = '" << albumName << "'))" <<
		"DELETE FROM PICTURES WHERE NAME = '" << pictureName << "' AND ALBUM_ID = (SELECT ID FROM ALBUMS WHERE NAME = '" << albumName << "')" <<
		"COMMIT;";
	this->execCommand(cmd.str());
}


void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	stringstream cmd;
	cmd << "INSERT INTO TAGS(PICTURE_ID, USER_ID) VALUES((SELECT ID FROM PICTURES WHERE NAME='" << pictureName
		<< "' AND ALBUM_ID=(SELECT ID FROM ALBUMS WHERE ALBUMS.NAME='" << albumName << "')), " << userId << ");";
	this->execCommand(cmd.str());
}



void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	stringstream cmd;
	cmd << "DELETE FROM TAGS WHERE PICTURE_ID=(SELECT ID FROM PICTURES WHERE NAME='" << pictureName <<
		"' AND ALBUM_ID=(SELECT ID FROM ALBUMS WHERE ALBUMS.NAME='" << albumName << "')) AND USER_ID='" << userId << "';";
	this->execCommand(cmd.str());
}

void DatabaseAccess::printUsers()
{
	stringstream cmd;
	cmd << "SELECT * FROM USERS;";
	std::list<User> listOfUsers;
	this->execSelectCmd(cmd.str(), this->usersCallback, &listOfUsers);

	for (std::list<User>::iterator i = listOfUsers.begin(); i != listOfUsers.end(); i++)
	{
		std::cout << "     + " << i->getName() << " with ID " << i->getId() << std::endl;
	}
}

User DatabaseAccess::getUser(int userId)
{
	stringstream cmd;
	cmd << "SELECT * FROM USERS where ID=" << userId << ";";
	std::list<User> listOfUsers;
	this->execSelectCmd(cmd.str(), this->usersCallback, &listOfUsers);

	User currentUser = listOfUsers.front();
	return currentUser;
}


void DatabaseAccess::createUser(User& user)
{
	stringstream cmd;
	cmd << "INSERT INTO USERS(NAME) VALUES('" << user.getName() << "');";
	this->execCommand(cmd.str());
}


void DatabaseAccess::deleteUser(const User& user)
{
	stringstream cmd;
	cmd << "BEGIN;" <<
		"DELETE FROM TAGS WHERE USER_ID = " << user.getId() << ";" <<
		"DELETE FROM TAGS WHERE PICTURE_ID IN(SELECT ID FROM PICTURES WHERE ALBUM_ID IN(SELECT USER_ID FROM ALBUMS WHERE USER_ID = " << user.getId() << "));" <<
		"DELETE FROM PICTURES WHERE ALBUM_ID IN(SELECT ID FROM ALBUMS WHERE USER_ID = " << user.getId() << ");" <<
		"DELETE FROM ALBUMS WHERE USER_ID = " << user.getId() << ";" <<
		"DELETE FROM USERS WHERE ID = " << user.getId() << ";" <<
		"COMMIT;";
	cmd << "DELETE FROM USERS WHERE ID = " << user.getId() << ";";
	this->execCommand(cmd.str());
}

bool DatabaseAccess::doesUserExists(int userId)
{
	stringstream cmd;
	cmd << "SELECT * FROM USERS where ID=" << userId << ";";
	std::list<User> listOfUsers;
	this->execSelectCmd(cmd.str(), this->usersCallback, &listOfUsers);

	return listOfUsers.size() != 0;
}


int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	stringstream cmd;
	double result = 0;
	cmd << "SELECT COUNT(ID) FROM ALBUMS WHERE USER_ID=" << user.getId() << ";";
	this->execSelectCmd(cmd.str(), this->singleFunctionalCallback, &result);

	return (int)result;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	stringstream cmd;
	double result = 0;
	cmd << "SELECT COUNT(ALBUM_ID) FROM (SELECT DISTINCT PICTURES.ALBUM_ID FROM " <<
		"PICTURES WHERE PICTURES.ID IN (SELECT PICTURE_ID FROM TAGS WHERE USER_ID=" << user.getId() << "));";
	this->execSelectCmd(cmd.str(), this->singleFunctionalCallback, &result);

	return (int)result;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	stringstream cmd;
	double result = 0;
	cmd << "SELECT COUNT(USER_ID) FROM TAGS WHERE USER_ID=" << user.getId() << ";";
	this->execSelectCmd(cmd.str(), this->singleFunctionalCallback, &result);

	return (int)result;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	stringstream cmd;
	cmd << "SELECT * from TAGS where USER_ID=" << user.getId() << ";";

	std::list<Tag> listOfTagsForUser;
	this->execSelectCmd(cmd.str(), this->tagsCallback, &listOfTagsForUser);

	stringstream cmd2;
	cmd2 << "SELECT * from TAGS;";
	std::list<Tag> listOfTagsAllUsers;
	this->execSelectCmd(cmd.str(), this->tagsCallback, &listOfTagsAllUsers);

	return float(listOfTagsForUser.size()) / listOfTagsAllUsers.size();
}

User DatabaseAccess::getTopTaggedUser()
{
	stringstream cmd;
	cmd << "SELECT * from TAGS group by USER_ID order by COUNT(*) DESC LIMIT 1;";
	std::list<Tag> listOfTags;
	this->execSelectCmd(cmd.str(), this->tagsCallback, &listOfTags);

	listOfTags.push_back(Tag());
	Tag mostTaggedUser = listOfTags.front();

	stringstream cmd2;
	cmd2 << "SELECT * from USERS where ID=" << mostTaggedUser.getUserID() << ";";
	std::list<User> listOfUsers;
	this->execSelectCmd(cmd2.str(), this->usersCallback, &listOfUsers);

	listOfUsers.push_back(User());
	User mostTaggedUserREAL = listOfUsers.front();

	return mostTaggedUserREAL;
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	stringstream cmd;
	cmd << "SELECT * from TAGS group by PICTURE_ID order by COUNT(*) DESC LIMIT 1;";
	std::list<Tag> listOfTags;
	this->execSelectCmd(cmd.str(), this->tagsCallback, &listOfTags);

	listOfTags.push_back(Tag());
	Tag mostTaggedPicture = listOfTags.front();

	stringstream cmd2;
	cmd2 << "SELECT * from PICTURES where ID=" << mostTaggedPicture.getPictureID() << ";";
	std::list<Picture> listOfPictures;
	this->execSelectCmd(cmd2.str(), this->picturesCallback, &listOfPictures);

	listOfPictures.push_back(Picture());
	Picture mostTaggedPictureREAL = listOfPictures.front();

	return mostTaggedPictureREAL;
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	stringstream cmd;
	cmd << "SELECT * FROM TAGS where USER_ID=" << user.getId() << ";";
	std::list<Tag> listOfTags;
	this->execSelectCmd(cmd.str(), this->tagsCallback, &listOfTags);

	std::list<Picture> listOfPictures;
	for (std::list<Tag>::iterator i = listOfTags.begin(); i != listOfTags.end(); i++)
	{
		std::list<Picture> listOfPicturesForNow;
		stringstream cmd2;
		cmd2 << "SELECT * FROM PICTURES where ID=" << i->getPictureID() << ";";
		this->execSelectCmd(cmd2.str(), this->picturesCallback, &listOfPicturesForNow);

		Picture currPic(listOfPicturesForNow.front().getId(), listOfPicturesForNow.front().getName(), listOfPicturesForNow.front().getPath(), listOfPicturesForNow.front().getCreationDate());
		listOfPictures.push_front(currPic);
	}

	return listOfPictures;
}


/*those func are takeing the DB data and put it in a chart */

int DatabaseAccess::albumsCallback(void* data, int argc, char** argv, char** azColName)
{
	list<Album>* now = (list<Album>*)data;
	Album currAlbum;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "NAME")
			currAlbum.setName(argv[i]);
		else if (string(azColName[i]) == "CREATION_DATE")
			currAlbum.setCreationDate(argv[i]);
		else if (string(azColName[i]) == "USER_ID")
			currAlbum.setOwner(atoi(argv[i]));
	}
	now->push_back(currAlbum);
	return 0;
}

int DatabaseAccess::usersCallback(void* data, int argc, char** argv, char** azColName)
{
	list<User>* now = (list<User>*)data;
	User currUser;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "ID")
			currUser.setId(atoi(argv[i]));
		else if (string(azColName[i]) == "NAME")
			currUser.setName(argv[i]);
	}
	now->push_back(currUser);
	return 0;
}

int DatabaseAccess::tagsCallback(void* data, int argc, char** argv, char** azColName)
{
	list<Tag>* now = (list<Tag>*)data;
	Tag currTag;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "PICTURE_ID")
			currTag.setPictureID(atoi(argv[i]));
		else if (string(azColName[i]) == "USER_ID")
			currTag.setUserID(atoi(argv[i]));
	}
	now->push_back(currTag);
	return 0;
}

int DatabaseAccess::picturesCallback(void* data, int argc, char** argv, char** azColName)
{
	list<Picture>* now = (list<Picture>*)data;
	Picture currPicture;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "NAME")
			currPicture.setName(argv[i]);
		else if (string(azColName[i]) == "LOCATION")
			currPicture.setPath(argv[i]);
		else if (string(azColName[i]) == "CREATION_DATE")
			currPicture.setCreationDate(argv[i]);
		else if (string(azColName[i]) == "ID")
			currPicture.setId(atoi(argv[i]));
	}
	now->push_back(currPicture);
	return 0;
}

int DatabaseAccess::singleFunctionalCallback(void* data, int argc, char** argv, char** azColName)
{
	double* output = (double*)data;
	*output = stod(argv[0]);
	return 0;
}



/*asist functions for run the SQL sqripts */

char* DatabaseAccess::stringToChArr(string& s) const
{
	char* p = new char[s.length() + 1];
	strcpy(p, s.c_str());
	return p;
}

void DatabaseAccess::execCommand(const char* cmd)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, cmd, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
		//if (errMessage != nullptr)
		throw std::exception(errMessage);
}

void DatabaseAccess::execCommand(string cmd)
{
	char* errMessage = nullptr;
	char* cmdCh = this->stringToChArr(cmd);
	int res = sqlite3_exec(this->_db, cmdCh, nullptr, nullptr, &errMessage);
	delete cmdCh;
	if (res != SQLITE_OK)
		//if (errMessage != nullptr)
		throw std::exception(errMessage);
}

void DatabaseAccess::execSelectCmd(const char* cmd, int (*callback)(void*, int, char**, char**), void* callbackParam = nullptr)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, cmd, callback, callbackParam, &errMessage);
	if (res != SQLITE_OK)
		//if (errMessage != nullptr)
		throw std::exception(errMessage);
}

void DatabaseAccess::execSelectCmd(string cmd, int (*callback)(void*, int, char**, char**), void* callbackParam = nullptr)
{
	char* errMessage = nullptr;
	char* cmdCh = this->stringToChArr(cmd);
	int res = sqlite3_exec(this->_db, cmdCh, callback, callbackParam, &errMessage);
	if (res != SQLITE_OK)
		//if (errMessage != nullptr)
		throw std::exception(errMessage);
}
