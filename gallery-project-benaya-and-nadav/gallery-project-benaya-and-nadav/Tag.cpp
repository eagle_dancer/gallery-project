#include "Tag.h"

Tag::Tag(int picID, int userID)
{
	this->m_pictureID = picID;
	this->m_userID = userID;
}

int Tag::getUserID() const
{
	return this->m_userID;
}

void Tag::setUserID(int newUserID)
{
	this->m_userID = newUserID;
}

int Tag::getPictureID() const
{
	return this->m_pictureID;
}

void Tag::setPictureID(int newPicID)
{
	this->m_pictureID = newPicID;
}
